Dagger module for working with debian packages

## Quickstart 

1. install [dagger CLI](https://docs.dagger.io/install)
2. call `dagger functions` 

## Examples 

Enter container with any salsa repo mounted 

```bash 
dagger call debug --src https://salsa.debian.org/levlaz-guest/lazarus.git#master terminal
```


