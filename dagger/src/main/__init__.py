"""Dagger module for debian packaging
"""

import dagger
from dagger import dag, function, object_type


@object_type
class SidBuilder:
    @function
    def base(self) -> dagger.Container:
        return  (
                dag
                .container()
                .from_("debian:stable")
                .with_exec(["apt-get", "update"])
                .with_exec(["apt-get", "upgrade", "-y"])
                .with_exec(["apt-get", "install", "-y", "git", "htop", "vim", "devscripts", "squid", "reprepro", "debmake", "apache2", "silversearcher-ag"])
                # fixme - need pbuilder mirror or something
                #.with_exec(["apt-get", "install", "-y", "pbuilder"])
                )

    @function 
    def debug(self, src: dagger.Directory) -> dagger.Container:
        """Returns container with directory mounted

        This can be a local dir or a remote git repo 
        """
        return (
                self.base()
                .with_mounted_directory("/src", src)
                .with_workdir("/src")
                )
