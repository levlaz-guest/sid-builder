# sid-builder

Docker based development environment using debian sid for packaging.

Most of the configuration options and tools are based off of the [Guide for Debian Maintainers](https://www.debian.org/doc/manuals/debmake-doc/index.en.html) document.

## Requirements

You  must have [Docker](https://store.docker.com/search?type=edition&offering=community) installed in order to use this project.

## Usage

1. Clone repo
2. `cd` into `sid-builder`
3. Go to the `Dockerfile` and change anything under `REPLACE`
4. Add your private gpg key to the root folder, it should have a `.asc` suffix and will automatically be ignored by `.gitignore`.
5. Run `docker-compose up -d`
6. Enter the container with `docker-compose exec debian bash`
7. Once you are inside the container, run `gpg --import $YOUR_PRIVATE_KEY`.
8. If you are going to start packaging, using the `build` directory inside of the container so that it does not obliterate the `.git` history of this repo.

## Updating

If you make any changes to the `Dockerfile` be sure to run `docker-compose up --build` in order to rebuild the image.

## Issues

I hope you find this useful. If you find a bug or have an idea for an enhancement please [file an issue in this project](https://salsa.debian.org/levlaz-guest/sid-builder/issues).